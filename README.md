# [symfony](https://phppackages.org/s/symfony)/[panther](https://phppackages.org/p/symfony/panther)

A browser testing and web crawling library for PHP and Symfony https://packagist.org/packages/symfony/panther

(Unofficial demo and howto)

[![PHPPackages Rank](http://phppackages.org/p/symfony/panther/badge/rank.svg)](http://phppackages.org/p/symfony/panther)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/panther/badge/referenced-by.svg)](http://phppackages.org/p/symfony/panther)

*  Inspired by [Nightwatch.js](http://nightwatchjs.org/)

## Official documentation
* [*Introducing Symfony Panther: a Browser Testing and Web Scrapping Library for PHP*
  ](https://symfony.com/blog/introducing-symfony-panther-a-browser-testing-and-web-scrapping-library-for-php)
* [*Announcing Symfony Panther 1.0*
  ](https://symfony.com/blog/announcing-symfony-panther-1-0)

## Unofficial documentation
* [*End-to-end testing with Symfony and Panther*
  ](https://www.strangebuzz.com/en/blog/end-to-end-testing-with-symfony-and-panther)
  2021-04 COil
